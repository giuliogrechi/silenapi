/**
 *   ##############################################
 *   #              Giulio Grechi                 #
 *   #   Corso di Sistemi di acquisizione dati    #
 *   #  driver di un ADC Silena per raspberryPi   #
 *   #                                            #
 *   ##############################################
*/

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/cdev.h>
#include <linux/gpio.h>
#include <asm/delay.h>
#include <linux/interrupt.h>
#include <linux/time.h>
#include <linux/uaccess.h>

#define BASE_MINOR 2
#define DEV_NUM 1
#define NAME "silPiGiul"
#define SIZE 1000
#define IRQPIO 26
#define ACCEPT 27
#define EVENTSIZE 16

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Giulio Grechi");
MODULE_DESCRIPTION("Silena device driver - working");
MODULE_VERSION("0.1");

/**
 * variabili globali per l'inizializzazione del device virtuale
 */
static dev_t device = 0;
static int cdev_flag = 0;
static struct cdev cdev;
static struct class * dev_class = NULL;
static struct device * dev_device = NULL;

/**
 * tiene traccia dell'overrun, è una variabile che può solo aumentare 
 * viene resettata solo con la rimozione del modulo.
 */
static int irqovr;
/**
 * inizializzazione della coda e della variabile per il mapping della 
 * linea di interrupt
 */
wait_queue_head_t queue;
static int irq;

/**
 * gli indici utilizzati per il buffer circolare devono essere definiti 
 * atomici, visto che vogliamo operare con interrupt
 */
atomic_t a_read_pointer;
atomic_t a_write_pointer;

/*
 * collegamenti fisici del dispositivo Silena ADC con i 
 * gpio a disposizione sul raspberryPi
 * 
 * {unsigned gpio, unsigned long flags, const char *label}
*/
struct gpio gpios[] = {
    {4,GPIOF_IN,"D00"},
    {5,GPIOF_IN,"D01"},
    {6,GPIOF_IN,"D02"},
    {7,GPIOF_IN,"D03"},
    {8,GPIOF_IN,"D04"},
    {9,GPIOF_IN,"D05"},
    {10,GPIOF_IN,"D06"},
    {11,GPIOF_IN,"D07"},
    {12,GPIOF_IN,"D08"},
    {13,GPIOF_IN,"D09"},
    {16,GPIOF_IN,"D10"},
    {18,GPIOF_IN,"D11"},
    {19,GPIOF_IN,"D12"},

    {IRQPIO,GPIOF_IN,"IRQ"},
    {ACCEPT,GPIOF_OUT_INIT_LOW,"RESET"},
};

/**
 * inizializzazione di un array di strutture, per immagazzinare i dati
 * raccolti.
 */
struct {
    struct timeval time;
    uint32_t value;
    uint32_t irqovr;
} events[SIZE];

/**
 * interrupt function
 * quando è ricevuto un interrupt, vengono letti i pin corrispondenti
 * agli ingressi, immagazzinando i dati all'interno del buffer
 * circolare e modificando opportunamente l'indice di scrittura dello stesso
 */
irqreturn_t irq_service(int irq, void * arg)
{
    int write_pointer;
    int val = 0;
    int j;

    /**
     * la linea di interrupt è tornata a 0, senza reset
     * dal software. Probabilmente è stato un glitch che ha causato
     * l'interrupt. Non c'è niente da leggere, la funzinoe ritorna
     */
    if(gpio_get_value(IRQPIO)==1){
        return IRQ_HANDLED;
    }
    
    write_pointer = atomic_read(&a_write_pointer);
    do_gettimeofday(&events[write_pointer].time);

    /**
     * converte un numero binario a 13 bit, MSB è connesso al gpio "D12"
     */
    for(j=12;j>=0;j--){
        val += val + gpio_get_value(gpios[j].gpio);
    }

    /**
     * il valore decimale è stato calcolato, adesso deve essere salvato 
     * in memoria e inviato un segnale di reset sulla linea ACCEPT.
     */
    gpio_set_value(ACCEPT,1);
    /**il valore letto è in logica negata */
    events[write_pointer].value = 0x1fff ^ val;
    events[write_pointer].irqovr = irqovr;
    /*incrementa di uno la variabile locale di write_pointer */
    write_pointer = (write_pointer + 1) % SIZE;

    if(write_pointer == atomic_read(&a_read_pointer)){
        /**
         * NON aggiorna il valore globale di write pointer ed aumenta di uno 
         * il valore di overrun. L'evento successivo sovrascriverà 
         * l'evento attuale.
         */
        irqovr++;
        printk(KERN_ALERT"%s - buffer overrun irqovr: %d\n",NAME,irqovr);
    }
    else{
        /** aggiorna il valore globale di write_pointer */
        atomic_set(&a_write_pointer,write_pointer);
    }
    gpio_set_value(ACCEPT,0);

    /**
     * wake_up has to be called after changing any variable that could
     * change the result of the wait condition.
     */
    wake_up(&queue);

    return IRQ_HANDLED;
}

int open(struct inode * inode, struct file * filep)
{
    int status;

    printk(KERN_ALERT"%s - gpio_request\n",NAME);
    status = gpio_request_array(gpios,ARRAY_SIZE(gpios));
    if(status){

        printk(KERN_ALERT
                     "%s - errore nella richiesta delle risorse gpio\n",NAME);
        return status;
    }

    /**
     * inizializzazione di una coda per permettere l'uso di 
     * wait_event_interruptible nella funzione di read
     * queue è definita come variabile globale:
     * wait_queue_head_t queue;
     */
    init_waitqueue_head(&queue);

    /**
     * il device è stato appena aperto, inizializzazione a zero dei puntatori 
     * di write e di read agli elementi del buffer circolare
     */
    atomic_set(&a_write_pointer,0);
    atomic_set(&a_read_pointer,0);
    irq = gpio_to_irq(IRQPIO); //mapping del pin IRQPIO
    
    /** alloca risorse e inizializza un interrupt sulla linea irq */
    status = request_threaded_irq(irq, irq_service,NULL, 
                                  IRQF_TRIGGER_FALLING, "silPiGiul", &irq);
    if(status){

        printk(KERN_ALERT"%s - errore nell'apertura degli interrupt\n",NAME);
        gpio_free_array(gpios,ARRAY_SIZE(gpios));
        return -1;
    }
    
    gpio_set_value(ACCEPT,1);
    udelay(10);
    gpio_set_value(ACCEPT,0);

    printk(KERN_ALERT"%s - device aperto\n",NAME);
    return 0;
}

int release(struct inode * inode, struct file * filep){
    free_irq(irq,&irq);
    gpio_free_array(gpios,ARRAY_SIZE(gpios));

    printk(KERN_ALERT"%s - device rilasciato\n",NAME);
    return 0;
}

int read(struct file * filep, char *buf, const size_t count, loff_t *ppos)
{
    uint32_t read_pointer;
    uint32_t retval;
    uint32_t write;
    uint32_t transfer;
    uint32_t request;
    uint32_t transfer_byte;
    uint32_t first_group;
    uint32_t second_group;

    read_pointer = atomic_read(&a_read_pointer);
    
    /** 
     * The process is put to sleep (wait_event_interruptible) until 
     * the condition evaluates to true or a signal is received. 
     * The condition is checked each time the waitqueue queue is woken up.
     * The function will return -ERESTARTSYS if it was interrupted by a signal 
     * and 0 if condition evaluated to true.
     */ 
    retval = wait_event_interruptible(
                            queue,
                            read_pointer!=(write=atomic_read(&a_write_pointer)));
    if(retval){
        printk(KERN_ALERT
               "%s - wait_event_interruptible interrotto dall'esterno\n",NAME);
        return retval;
    }

    /**
     * l'utente ha richiesto di leggere un numero di byte pari a count,
     * quindi un numero di eventi (request) pari a count/EVENTSIZE
     * se il numero di eventi è minore di quelli richiesti, se ne 
     * invia un numero inferiore, altrimenti il numero richiesto
     * 
     * il buffer è circolare, ci sono quindi due possibilità:
     * 1. che i dati da trasferire siano contigui
     * 2. che i dati occupino la porzione finale ed iniziale 
     * 
     *  1. |--------xxxxxxxxxxxxxxxxxxx--------------------|
     *              ^rp               ^wp
     *  
     *  2. |xxxxxxxx---------------------------xxxxxxxxxxxx|
     *             ^wp                         ^rp               
     */

    /**numero di eventi ancora non letti nel buffer */
    transfer = (write + SIZE - read_pointer)%SIZE;
    request = count/EVENTSIZE;
    if(transfer>request) {
        /**sono disponibili più eventi di quelli richiesti*/
        transfer = request;
    }
    
    transfer_byte = transfer*EVENTSIZE;
    
    if(read_pointer+transfer<=SIZE){
        /**copia all'indirizzo di memoria puntato da buf IN USER SPACE,
         * un numero di byte pari a transfer_byte, letti nella regione
         * di memoria KERNEL SPACE puntata da (events + read_pointer)
         * (che equivale a events[read_pointer])
         */
        retval = copy_to_user(buf,events + read_pointer, transfer_byte);
        if(retval){
            printk(KERN_ALERT"%s - something went wrong - retval: %d\n",NAME,retval);
            goto copy_error;
        }
    }else{
        /**
         * copia dei dati in due blocchi
         */
        first_group = (SIZE - read_pointer)*EVENTSIZE;
        retval = copy_to_user(buf,events + read_pointer,first_group);
        if(retval){
            printk(KERN_ALERT"%s - something went wrong - retval: %d\n",NAME,retval);
            goto copy_error;
        }
        
        second_group = transfer_byte - first_group;
        retval = copy_to_user(buf+first_group,events,second_group);
        if(retval){
            printk(KERN_ALERT"%s - something went wrong - retval: %d\n",NAME,retval);
            goto copy_error;
        }
    }
    
    /**aggiornamento del read_pointer */
    read_pointer = (read_pointer + transfer)%SIZE;
    atomic_set(&a_read_pointer,read_pointer);
    
    return transfer_byte;
    
    copy_error:
        return -EFAULT;
}


int write(struct file * filep, const char *buf, size_t count, loff_t *ppos)
{
    printk(KERN_ALERT"%s - scrittura di %d bytes\n",NAME,count);
    return count;
}

/**
 * operazioni sul device virtuale rese disponibili all'utente 
 */
static struct file_operations fops = {
    .owner = THIS_MODULE,
    .open = open,
    .release = release,
    .read = read,
    .write = write,
};

/**
 * funzione di exit
 * rimuove il device e dealloca la memoria associata
 * in ordine inverso rispetto a come sono stati creati
 */
static void modGiul_exit(void)
{
    printk(KERN_ALERT"%s - rimozione del modulo\n",NAME);
    if(dev_device) {
        device_destroy(dev_class,device);
    }
    if(dev_class){
        class_destroy(dev_class);
    }
    if(cdev_flag){
        cdev_del(&cdev);
    }
    if(device){
        unregister_chrdev_region(device,DEV_NUM);
    }
    printk(KERN_ALERT"%s - modulo rimosso correttamente\n",NAME);
}

/** funzione di init */
static int modGiul_init(void)
{
    long status = 0;
    
    /*
     * alloc_chrdev_region — register a range of char device numbers 
     * Allocates a range of char device numbers. The major number will be 
     * chosen dynamically, and returned (along with the first minor number) 
     * in dev. Returns zero or a negative error code. 
     * 
     * dev_t * device       - output parameter for first assigned number 
     * unsigned BASE_MINOR  - first of the requested range of minor numbers 
     * unsigned DEV_NUM     - the number of minor numbers required 
     * const char * NAME    - the name of the associated device or driver 
     */
    status = alloc_chrdev_region(&device,BASE_MINOR,DEV_NUM,NAME);
    if(status < 0){
        printk(KERN_ALERT"%s - failure in alloc_chrdev\n",NAME);
        return status;
    }
    printk(KERN_ALERT"%s - allocata memoria - major is %d\n",
           NAME,
           MAJOR(device)
           );

    /**
     * cdev_init — initialize a cdev structure 
     * Initializes cdev, remembering fops, making it ready 
     * to add to the system with cdev_add. 
     * 
     * struct cdev * cdev                  - the structure to initialize 
     * const struct file_operations * fops - the file_operations for this device 
    */
    cdev_init(&cdev,&fops);
    cdev.owner = THIS_MODULE;

    /**
     * cdev_add — add a char device to the system 
     * cdev_add adds the device represented by cdev to the system, 
     * making it live immediately. A negative error code is returned on failure. 
     * 
     * struct cdev * cdev - the cdev structure for the device 
     * dev_t device       - the first device number for 
     *                      which this device is responsible  
     * unsigned DEV_NUM   - the number of consecutive minor numbers 
     *                      corresponding to this device 
     */
    status = cdev_add(&cdev,device,DEV_NUM);
    if(status < 0){
        printk(KERN_ALERT"%s - failure in cdev_add\n",NAME);
        goto failure;
    }
    cdev_flag = 1;

    /**
     * To use a character driver, first you should register it with the system. 
     * Then you should expose it to the user space.
     * 
     * cdev_init and cdev_add functions perform the character device registration. 
     * cdev_add adds the character device to the system. 
     * When cdev_add function successfully completes, the device is live and 
     * the kernel can invoke its operations.
     * 
     * In order to access this device from user space, you should create a 
     * device node in /dev. You do this by creating a virtual device class
     * using class_create, then creating a device and registering it with sysfs 
     * using the device_create function. device_create will create 
     * a device file in /dev.
     */

    dev_class = class_create(THIS_MODULE,NAME);
    if(IS_ERR(dev_class)){
        printk(KERN_ALERT"%s - failure in class_create\n",NAME);
        goto failure;
    }
    printk(KERN_ALERT"%s - class_create eseguito correttamente\n",NAME);

    dev_device = device_create(dev_class,NULL,device,NULL,NAME);
    if(IS_ERR(dev_device)){
        printk(KERN_ALERT"%s - failure in device_create\n",NAME);
        goto failure;
    }
    printk(KERN_ALERT"%s - device_create eseguito direttamente\n",NAME);

    return 0;

    failure:
        modGiul_exit();
        return -1;
}

module_init(modGiul_init);
module_exit(modGiul_exit);
