#!/usr/bin/env python

import os
import subprocess
import zmq
import time
from sys import exit
from struct import unpack

device = "/dev/silPiGiul"
hist_file = "hist_file.dat"
DATA_SIZE = 16
data_format = 'IIII' #testato 'iiii, funziona, ma 'IIII' dovrebbe essere piu' 
                     #corretto, visto che sono 4 uint32_t
adc_res = 4096
num_to_send = 1000



def main():
    context = zmq.Context()
    socket = context.socket(zmq.PUB)
    socket.bind("tcp://*:5555")
    
    try:
        index = 0
        buffer_list = [0]*num_to_send
        buffer_hist = [0]*adc_res
        dev = open(device,'r')
        print("device aperto")

        while True:
            data = dev.read(DATA_SIZE)
            if(len(data)==DATA_SIZE):
                buffer_data = unpack(data_format,data)
                #print(buffer_data)
                buffer_hist[buffer_data[2]] += 1

                buffer_list[index] = '%04.0d'%buffer_data[2]
                index += 1
                if index == num_to_send:
                    index = 0
                    socket.send_string( ' '.join(buffer_list) )
                    print("Sent %d numbers - %s"%(num_to_send,time.time()))
                    with open(hist_file, 'w') as hf:
                        hf.write("#code counts %s\n"%time.asctime(
                                                 time.localtime(time.time())))
                        for i,c in enumerate(buffer_hist):
                            hf.write("%04.0d %d\n"%(i,c))
            else:
                print("letti un numero di byte minore di %.0d"%DATA_SIZE)
                raise ValueError('Read less than %.0d bytes'%DATA_SIZE)

    except KeyboardInterrupt, ValueError:
        print ValueError
        dev.close()
        exit(0)

if __name__ == "__main__":
    main()