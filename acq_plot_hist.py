import zmq
import ROOT
import time
from sys import exit

adc_res = 4096
data_file_hist = "histogram_file.dat"

def main():
    hist_buffer = [0]*adc_res

    print("Connecting to server...")
    context = zmq.Context()
    socket = context.socket(zmq.SUB)
    socket.connect("tcp://localhost:5555")
    socket.setsockopt_string(zmq.SUBSCRIBE, u'')

    canvas = ROOT.TCanvas()
    histogram = ROOT.TH1I("h1", "Spettro Ambientale", adc_res,0,adc_res)
    histogram.GetXaxis().SetTitle("adc code")
    histogram.GetYaxis().SetTitle("counts")
    histogram.Draw()

    i = 0
    while True:
        datas = [int(x) for x in socket.recv_string().split(' ')]
        i = i + 1
        print("Received reply %s [ %s lunghezza: %d]" % (i, datas[0],len(datas)))
        for dat in datas:
            hist_buffer[int(dat)] += 1
            histogram.Fill(dat)
        canvas.Modified()
        canvas.Update()
        if i%10 == 0:
            with open(data_file_hist,'w') as hst:
                hst.write("#bin counts\t%s\n"%time.asctime(time.localtime(time.time())))
                for j,c in enumerate(hist_buffer):
                    hst.write("%d %d\n"%(j,c))
            print("Salvato file istogramma")
    return

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        exit(0)
